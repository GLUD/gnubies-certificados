#!/bin/zsh

echo "-------- Iniciando instalación -------------"

#Creando carpeta 
mkdir /usr/share/fonts/william-morittan
echo "Carpeta creada"

#Copiando fuente a carpeta creada
cp WilliamMorittan.ttf /usr/share/fonts/william-morittan
echo "Archivo copiado"

#Cambiando permisos para la fuente
chmod ugo+w /usr/share/fonts/william-morittan/WilliamMorittan.ttf
echo "Permisos cambiados"

echo "------------ Instalación terminada -----------"

