#!/bin/zsh

echo "\e[32m---------------Iniciando SCRIPT--------------"

echo -e "\e[32mLeyendo el nombre del director ... "
read director

echo -e "\e[32mLeyendo el nombre del tutor ... "
read tutor

echo -e "\e[32mLeyendo la firma del tutor ... "
read firmat

echo -e "\e[32mLeyendo la fecha de expedicion (ejemplo: 27 de febrero del 2021) ...  "
read fecha

echo -e "\e[32mLeyendo el número de estudiantes ... "
read n

rm -rf certificados
mkdir certificados && cd certificados

for (( i=0;i<n;i++ ))
do
	echo -e "\e[32mLeyendo el nombre del estudiante número $(( $i+1 )) ... "
	read estudiante;
	cp   ../certificado_individual.svg ./$estudiante.svg

	archivo=$estudiante.svg
	sed -i 's/Directore/'$director'/g' $archivo
	sed -i 's/Tutor/'$tutor'/g' $archivo
    sed -i 's/Firmat/'$firmat'/g' $archivo
	sed -i 's/Fecha/'$fecha'/g' $archivo
	sed -i 's/Nombre/'$estudiante'/g' $archivo
    inkscape --export-pdf=$estudiante.pdf $archivo 2> /dev/null
done

rm *.svg

echo "\e[32m----------- CERTIFICADOS CREADOS ------------------"
