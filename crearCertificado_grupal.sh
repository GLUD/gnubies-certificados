#!/bin/zsh

echo "\e[32m---------------Iniciando SCRIPT--------------"

echo -e "\e[32mLeyendo el nombre del director ... "
read director

echo -e "\e[32mLeyendo el nombre del primer tutor ... "
read tutor_1

echo -e "\e[32mLeyendo la firma del primer tutor ... "
read firma_1

echo -e "\e[32mLeyendo el nombre del segundo tutor ... "
read tutor_2

echo -e "\e[32mLeyendo la firma del segundo tutor ... "
read firma_2

echo -e "\e[32mLeyendo la fecha de expedicion (ejemplo: 27 de febrero del 2021) ...  "
read fecha

echo -e "\e[32mLeyendo el número de estudiantes ... "
read n

rm -rf certificados
mkdir certificados && cd certificados

for (( i=0;i<n;i++ ))
do
	echo -e "\e[32mLeyendo el nombre del estudiante número $(( $i+1 )) ... "
	read estudiante;
	cp   ../certificado_grupal.svg ./$estudiante.svg

	archivo=$estudiante.svg
	sed -i 's/Directore/'$director'/g' $archivo
	sed -i 's/Tutor_1/'$tutor_1'/g' $archivo
    sed -i 's/Firma_1/'$firma_1'/g' $archivo
	sed -i 's/Tutor_2/'$tutor_2'/g' $archivo
    sed -i 's/Firma_2/'$firma_2'/g' $archivo
	sed -i 's/Fecha/'$fecha'/g' $archivo
	sed -i 's/Nombre/'$estudiante'/g' $archivo
    inkscape --export-pdf=$estudiante.pdf $archivo 2> /dev/null
done

rm *.svg

echo "\e[32m----------- CERTIFICADOS CREADOS ------------------"
