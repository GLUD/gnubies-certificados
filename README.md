# Certificados GNUBIES

[![Andrés David Becerra D.](https://img.shields.io/badge/andabed-gitlab-br?style=flat-square)](https://gitlab.com/andabed)
[![Juan Sebastian Romero](https://img.shields.io/badge/JuanseK-github-br?style=flat-square)](https://gitlab.com/juanse-k)
[![Juan Felipe Rodriguez Galindo](https://img.shields.io/badge/Juferoga-github-br?style=flat-square)](https://github.com/Juferoga)
[![License](https://img.shields.io/badge/License-GPL_V.3-blue?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.html)

<img src="https://gitlab.com/GLUD/gnubies-certificados/-/raw/master/assets/certificado.png" width="auto" height="auto" align="center"/>
<hr>

## Contributors

* Andrés David Becerra  - **COD. 20192020074** <br>
* Juan Sebastian Romero - **COD. 20192020067** <br>
* Juan Felipe Rodríguez - **COD. 20181020158** <br>


# Certificados GNUBIES

Este repositorio genera los certificados para los newbies que pasen el curso. Para que funcione el script es importante tener instalado *inkscape* y tener como interprete de comandos *zsh*.

## Instrucciones

1) Verificar si posee inkscape, zsh y las fuente necesarias.

2) Creer un archivo de texto con el nombre `lista.txt` he ingrese la informacion que se solicita en el archivo `gnubies_ind` o en `gnubies_grupal`.

3) Ejecutar el siguiente comando: 
- `cat lista.txt | ./crearCertificado_ind.sh` 
- `cat lista.txt | ./crearCertificado_grupal.sh`

## Instalando Inkscape 

Debian `sudo apt-get install inkscape`

Arch   `sudo pacman -S inkscape` confirmar paquetes extra!

## Instalando zsh y oh my zsh

Debian `sudo apt-get install zsh`

Arch   `sudo pacman -S zsh` confirmar paquetes extra!

### ohmyzsh

para instalar ohmyzsh copy and paste

`sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"`

## Instalando fuente

Entrar al directorio `fonts` y ejecutar como administrador el script `instalarFuente.sh` de la siguiente forma `sudo ./instalarFuente.sh`
